# mandarin_wav2vec2

checkpoint里面pytorch_model.bin模型文件370M，超出gitee上LFS限制，请到阿里云下载，如下：

git clone https://lczjg:wav2vec2@codeup.aliyun.com/65731d693e469c2f3534ba82/mandarin_wav2vec2.git

用户名：lczjg

密码：wav2vec2

#### 介绍
基于语音表征的普通话语音识别

#### 软件架构
pytorch下fairseq


#### 安装教程

1.  搭建pytorch开发环境
2.  安装soundfile
3.  安装transformers

#### 使用说明

1.  把整个工程下载到本地
2.  打开predict_transformer.py运行

#### 参与贡献

1.  张金光创建本仓库
2.  2023年09月23日提交代码



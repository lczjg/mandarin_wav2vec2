# -*- coding: UTF-8 -*- ----------pytorch-fairseq----------#

import soundfile as sf
import torch
from transformers import Wav2Vec2ForCTC, Wav2Vec2Processor

language_model = r"./checkpoint"

from_str = ['+','!','@','#','-','^','v$','$','jv','qv','xv']
to_str = ['i','zh','ch','sh','i','er','iong','ng','ju','qu','xu']

processor = Wav2Vec2Processor.from_pretrained(language_model)
model = Wav2Vec2ForCTC.from_pretrained(language_model)

audio_input, _ = sf.read(r'./测试语音/bai_yang.wav')
input_values = processor(audio_input, sampling_rate=16_000, return_tensors="pt").input_values
logits = model(input_values).logits
predicted_ids = torch.argmax(logits, dim=-1)
trans = processor.batch_decode(predicted_ids)[0]

for i in range(len(from_str)):
    trans = trans.replace(from_str[i],to_str[i])
print(trans)
